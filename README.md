# A simple shopping cart for Laravel


### Methods

- `Cart::add($item_id [, $quantity=1])` : `Cart` Add an item to the cart
- `Cart::quantity($item_id, $quantity)` : `Cart` Set the quantity for item
- `Cart::remove($item_id)` : `Cart` Remove a item from the cart
- `Cart::destroy()` : `Cart` Delete cart
- `Cart::get()` : `Array` Save the cart and items in the session or database and returns an array with cart data


### Usage

Add the Facade Cart
```
use Cart;
```

Add an item to the cart

```
// Quantity default = 1
$ArrayCart = Cart::add(1)->get();

// Quantity = 2
$ArrayCart = Cart::add(1, 2)->get();
```

Add multiple items to the cart

```
// Quantity default = 1
$ArrayCart = Cart::add([1 ,2, 3])->get();

// [item_id => quantity]
$ArrayCart = Cart::add([1 => 5, 2 => 1, 3 => 2])->get();
```


Set the quantity for item
```
$ArrayCart = Cart::quantity(1, 5)->get();
```

Get the total price of the items in the cart
```
$cart = Cart::add(1, 5);
$cart->add(2);
$ArrayCart = $cart->get();

$total = $ArrayCart['total'];

```

Remove a item from the cart
```
$ArrayCart = Cart::remove(1)->get();

```

Delete cart
```
$ArrayCart = Cart::destroy(1);
```