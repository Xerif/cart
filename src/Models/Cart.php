<?php

namespace Xerifandtomas\Cart\Models;

use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
    ];

    public function details()
    {
        return $this->hasMany('Xerifandtomas\Cart\Models\Detail');
    }
}
