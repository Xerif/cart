<?php

namespace Xerifandtomas\Cart\Models;

use Illuminate\Database\Eloquent\Model;

class Detail extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'cart_id',
        'item_id',
        'quantity',
    ];

    public function cart()
    {
        return $this->belongsTo('Xerifandtomas\Cart\Models\Cart');
    }
}
