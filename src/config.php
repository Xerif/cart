<?php

return [
    'table_items'  => env('TABLE_ITEMS', 'products'),
    'column_price' => env('COLUMN_PRICE', 'price'),
    'primary_key'  => env('PRIMARY_KEY', 'id'),
    'class_item'   => env('CLASS_PRODUCT', '\App\Product'),
];