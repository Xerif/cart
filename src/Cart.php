<?php

namespace Xerifandtomas\Cart;

use Xerifandtomas\Cart\Models\Cart as CartModel;
use Xerifandtomas\Cart\Models\Detail as DetailModel;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class Cart
{
    protected $items   = array();
    protected $tempAdd = array();
    protected $tempDel = array();
    protected $tempQua = array();
    protected $user_id = null;
    protected $cart_id = null;

    /**
    * Set the config
    *
    * @return void
    */
    public function __construct()
    {
        $this->table = config('cart.table_items', 'products');
        $this->pk = config('cart.primary_key', 'id');
        $this->column_price = config('cart.column_price', 'price');
        $this->classItem = config('cart.class_item', '\App\Product');
    }

    protected function getClassItem()
    {
        return $class = new $this->classItem;
    }

    /**
    * Add an item to the cart
    *
    * @param int | array    $item_id
    * @param int    $quantity
    * @return Xerifandtomas\Cart\Cart
    */
    public function add($item_id, $quantity=1): self
    {
        if(is_array($item_id)) {
            $items = $this->getClassItem()->whereIn($this->pk, array_keys($item_id))->get();
            $items = $items->map(function ($item) use ($item_id) {
                $item->quantity = $item_id[$item->{$this->pk}];
                return $item;
            });
        } else {
            $item = $this->getClassItem()->where($this->pk, $item_id)->first();
            $item->quantity = is_numeric($quantity) ? round($quantity) : 1; 
            $items = [$item->{$this->pk} => $item];           
        }

        foreach ($items as $item_id => $item) {
            if( array_key_exists($item->{$this->pk}, $this->items) ) {
                $this->items[$item->{$this->pk}]->quantity += $item->quantity;
            } else {
                $this->items[$item->{$this->pk}] = $item;
            }

        }

        return $this;
    }

    /**
    * Proposes an amount to set for an item
    *
    * @param int    $item_id
    * @param int    $quantity
    * @return Xerifandtomas\Cart\Cart
    */
    public function quantity($item_id, $quantity=1): self
    {
        if($quantity < 1){
            $this->remove($item_id);
        } else {
            $this->tempQua[$item_id] = $quantity;
        }
        return $this;
    }

    /**
    * Propose an item to be deleted
    *
    * @param int    $item_id
    * @return Xerifandtomas\Cart\Cart
    */
    public function remove($item_id, $quantity=null): self
    {
        if($quantity AND $quantity > 0) {
            $quantity = $quantity * (-1);
        }

        $this->tempDel[$item_id] = $quantity;
        return $this;
    }

    /**
    * Delete cart
    *
    * @return Xerifandtomas\Cart\Cart
    */
    public function destroy(): self
    {
        Session::forget('cart');
        CartModel::where('id', $this->getCartId())->delete();
        $this->get();
        return $this;
    }

    /**
    * Set the quantity for items
    *
    * @param array    $items
    * @return Xerifandtomas\Cart\Cart
    */
    protected function setQuantity(): self
    {
        foreach ($this->tempQua as $item_id => $quantity) {
            $quantity = is_numeric($quantity) ? round($quantity) : 1;

            if(array_key_exists($item_id, $this->items) ) {
                $this->items[$item_id]->quantity = $quantity;
            }
        }

        return $this;
    }

    /**
    * Propose an item for the cart
    *
    * @param int    $item_id
    * @param int    $quantity
    * @return Xerifandtomas\Cart\Cart
    */
    protected function setQuantityHigher($item_id, $quantity): self
    {
        if(!array_key_exists($item_id, $this->tempAdd) ) {
            $this->tempAdd[$item_id] = $quantity;
        } elseif($this->tempAdd[$item_id] < $quantity) {
            $this->tempAdd[$item_id] = $quantity;
        }
        return $this;
    }

    /**
    * Get the total price of the items in the cart
    *
    * @return int|float
    */
    protected function total(): float
    {
        $sum = 0;
        foreach ($this->items as $item) {
            $sum += $item->{$this->column_price} * $item->quantity;
        }
        return $sum;
    }

    /**
    * Get the number of the items in the cart
    *
    * @return int
    */
    protected function count(): int
    {
        return count($this->items);
    }

    /**
    * Remove items with an amount less than zero or adjust the quantity
    *
    * @return Xerifandtomas\Cart\Cart
    */
    protected function garbageDeletion(): self
    {
        foreach ($this->tempDel as $item_id => $quantity) {
            unset($this->items[$item_id]);
        }
        return $this;
    }

    /**
    * Get items in session
    *
    * @return Xerifandtomas\Cart\Cart
    */
    protected function getSession(): self
    {
        if(Session::has('cart')) {
            foreach (Session::get('cart')['items'] as $detail) {
                $this->setQuantityHigher($detail->id, $detail->quantity);
            }
        }
        return $this;
    }

    /**
    * Get items in database
    *
    * @return Xerifandtomas\Cart\Cart
    */
    protected function getDatabase(): self
    {
        if(!$this->getUserId()) {
            return $this;
        } else if($details = DetailModel::where('cart_id', $this->getCartId())->orderBy('updated_at')->get()) {
            foreach ($details as $detail) {
                $this->setQuantityHigher($detail->item_id, $detail->quantity);
            }
        }
        return $this;
    }

    /**
    * Get user id
    *
    * @return int | null
    */
    protected function getUserId()
    {
        if($this->user_id > 0) {
            return $this->user_id;
        }
        return $this->user_id = Auth::user() ? Auth::user()->id : null;
    }

    /**
    * Get cart id
    *
    * @return int | null
    */
    protected function getCartId()
    {
        if(!$this->getUserId()) {
            return $this->cart_id = null;
        }else if($this->cart_id > 0) {
            return $this->cart_id;
        }else if($cart = CartModel::where('user_id', $this->getUserId())->orderBy('updated_at')->first()) {
            return $this->cart_id = $cart->id;
        }

        $cart = CartModel::create(['user_id' => $this->user_id]);

        return $this->cart_id = $cart->id;
    }

    /**
    * Set items in session
    *
    * @param array    $cart
    * @return Xerifandtomas\Cart\Cart
    */
    protected function putSession($cart): self
    {
        Session::put('cart', $cart);
        return $this;
    }

    /**
    * Set items in database
    *
    * @param array    $items
    * @return Xerifandtomas\Cart\Cart
    */
    protected function putDatabase($items): self
    {
        if (!$this->getUserId() OR !$this->getCartId()) {
            return $this;
        }

        $cart_id = $this->cart_id;

        $items = array_map(function($item) use ($cart_id) {
            return array(
                'item_id'  => $item->id,
                'quantity' => $item->quantity,
                'cart_id'  => $cart_id,
                'created_at' => new \dateTime,
                'updated_at' => new \dateTime,
            );
        }, $items);

        DetailModel::where('cart_id', $cart_id)->delete();
        DetailModel::insert($items);

        return $this;
    }

    /**
    * Save the cart and items in the session and database. Returns an array with cart data
    *
    * @return array
    */
    public function get(): array
    {
        $this->getDatabase()
            ->getSession()
            ->add($this->tempAdd)
            ->setQuantity()
            ->garbageDeletion();

        $cart = [
            'user_id' => $this->getUserId(),
            'cart_id' => $this->getCartId(),
            'total'   => $this->total(),
            'count'   => $this->count(),
            'items'   => $this->items,
        ];

        $this->putSession($cart)
            ->putDatabase($this->items);

        $this->items = array();
        $this->tempAdd = array();
        $this->tempDel = array();
        $this->tempQua = array();

        return $cart;
    }
}
